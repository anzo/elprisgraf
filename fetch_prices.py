from io import BytesIO
import json
import os
from dotenv import load_dotenv
from flask import Flask, send_file, render_template
import pandas as pd
import plotly.graph_objects as go
import plotly.utils as utils
import plotly.express as px
import requests

load_dotenv()
app = Flask(__name__)

def translate_price_level(price_level):
    translated_name = ""
    if price_level == 'VERY_CHEAP':
        translated_name = 'Väldigt billigt'
    elif price_level == 'CHEAP':
        translated_name = 'Billigt'
    elif price_level == 'NORMAL':
        translated_name = 'Normalt'
    elif price_level == 'EXPENSIVE':
        translated_name = 'Dyrt'
    else:
        translated_name = 'Väldigt dyrt'

    return translated_name

def fetch_prices():
    url = 'https://api.tibber.com/v1-beta/gql'
    headers = {'Authorization': os.environ['AUTH_TOKEN']}
    homeId = os.environ['HOME_ID']
    payload = {"query": f'{{ viewer {{ home(id: "{homeId}") {{ currentSubscription {{ priceInfo {{today {{total, startsAt, level}}}}}}}}}}}}'}
    r = requests.post(url, headers=headers, json=payload)

    data = r.json()['data']['viewer']['home']['currentSubscription']['priceInfo']['today']
    return pd.DataFrame({
        'Price': [y['total']*100 for y in data],
        'Hour': [x['startsAt'] for x in data],
        'PriceLevel': [translate_price_level(item['level']) for item in data]
    })

@app.route('/graf.html')
def make_interactive_chart():
    df = fetch_prices()
    
    fig = px.bar(
        df,
        x='Hour',
        y='Price',
        text_auto='.0f',
        labels={'Price':'Pris (öre)', 'Hour': 'Timme'},
        color='PriceLevel',
        color_discrete_map={
                "Väldigt billigt": "#23B8CC",
                "Billigt": "#159EB5",
                "Normalt": "#107C92",
                "Dyrt": "#075369",
                "Väldigt dyrt": "#003B4F"},
    )
    fig.update_layout({
        'plot_bgcolor': '#D9F7FF',
        'paper_bgcolor': 'white',
    })

    graphJSON = json.dumps(fig, cls=utils.PlotlyJSONEncoder)

    return render_template('graph.html', graphJSON=graphJSON)


@app.route('/graf.png')
def make_chart():
    df = fetch_prices()
    fig = px.line(df, x='Hour', y='Price')
    fig.show()

    image_bytes = fig.to_image(format="png", engine="kaleido")

    return send_file(
        BytesIO(image_bytes),
        download_name='graph.png',
        mimetype='image/png'
    )