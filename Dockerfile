FROM python:3.10-slim-bullseye
RUN pip install flask plotly requests kaleido python-dotenv pandas
RUN mkdir /code
WORKDIR /code
COPY . /code
EXPOSE 5001
CMD flask --app fetch_prices:app --debug run --host 0.0.0.0 --port 5001